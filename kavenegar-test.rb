require 'httparty'

receptor = ENV['phone_number'] 
api_key = ENV['kavenegar_key']

url = "http://api.kavenegar.com/v1/#{api_key}/sms/send.json" 
body = {receptor: receptor, message: "This is a message from a ruby code. "}

response = HTTParty.post(url, body: body)

puts "The response code is: #{response.code}"
#puts response.body 
 